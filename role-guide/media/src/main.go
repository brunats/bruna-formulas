// This is the main class.
// Where you will extract the inputs asked on the config.json file and call the formula's method(s).

package main

import (
	"formula/pkg/formula"
	"os"
)

func main() {
	inputAba := os.Getenv("input_nome_aba")
	inputColunasPerformance := os.Getenv("input_colunas_performance")
	inputColunasNivel := os.Getenv("input_colunas_nivel")
	inputColunasTrabalho := os.Getenv("input_colunas_trabalho")
	inputColunasCultura := os.Getenv("input_colunas_cultura")

	formula.Formula{
		InputAba:                inputAba,
		InputColunasPerformance: inputColunasPerformance,
		InputColunasNivel:       inputColunasNivel,
		InputColunasTrabalho:    inputColunasTrabalho,
		inputColunasCultura:     inputColunasCultura,
	}.Run()
}
